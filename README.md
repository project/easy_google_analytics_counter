# Easy Google Analytics Counter

Drupal module get pageview data from Google analytics.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/easy_google_analytics_counter).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/easy_google_analytics_counter).


## Getting started

After install and configure module new column is been created on node_field_data
with name page_views. This column ready to using from views as field, filter and
sort data.


## Requirements

Module needs the Google Analytics Data (google/analytics-data) library. If you install module with
composer the library will download also.

If you use other technology to install module you need to install this library
manualy.

In the project (not module!) root folder run this command:
```
composer require "google/analytics-data":"^0.11.1"
```


## Installation

Install module with composer:
```
composer require drupal/easy_google_analytics_counter
```


## Configuration

- Go to 
```
https://github.com/googleapis/google-cloud-php/blob/main/AUTHENTICATION.md
```
and follow the todo.
- Go to YOURSITE/admin/config/easy_google_analytics_counter/admin and enter data
  and save.
- Set cron to periodicaly running on hosting server.
- Prepare or update views to using pageviews.


## Author

Dudas Jozsef dj@brainsum.com


## Sponsored

* Tieto - [Tieto](https://www.tieto.com)
* Brainsum - [Brainsum](https://www.brainsum.com)
