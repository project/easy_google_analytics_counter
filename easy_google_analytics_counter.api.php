<?php

/**
 * @file
 * easy_google_analytics_counter.api.php
 *
 * Author: dj
 * created: 2019.01.04. - 15:48:16
 *
 * The easy_google_analytics_counter api file.
 */

/**
 * Altering Report Query.
 *
 * @param array $query
 *   The request object.
 */
function hook_easy_google_analytics_counter_query_alter(array $query) {
  // Add page title to dimensions.
  $query['dimensions'][] = ['name' => 'ga:pageTitle'];
}

/**
 * Let developers expend the updated page views data.
 *
 * @param array $update_nids
 *   An associative array the page views keyed by node id.
 */
function hook_easy_google_analytics_counter_update_node_page_views(array $update_nids) {
  // Foreach ($update_nids as $nid => $page_views) {
  // Do something.
  // }.
}
