<?php

namespace Drupal\easy_google_analytics_counter;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * The ConnectionServiceInterface interface.
 */
interface ConnectionServiceInterface {

  /**
   * Prepare the google analytics request.
   *
   * @param string $page_path
   *   The requested page path.
   */
  public function request(string $page_path = '');

  /**
   * Return Google Analytics config.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The configuration.
   */
  public function config(): ImmutableConfig;

  /**
   * Return the logger.
   *
   * @return \Drupal\Core\Logger\LoggerChannelFactoryInterface
   *   The logger.
   */
  public function logger(): LoggerChannelFactoryInterface;

  /**
   * Return the module handler.
   *
   * @return \Drupal\Core\Extension\ModuleHandlerInterface
   *   The module handler.
   */
  public function moduleHandler(): ModuleHandlerInterface;

}
