<?php

namespace Drupal\easy_google_analytics_counter;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

// --------- Use from google analytics library ----------.
use Google\Analytics\Data\V1beta\BetaAnalyticsDataClient;
use Google\Analytics\Data\V1beta\DateRange;
use Google\Analytics\Data\V1beta\Dimension;
use Google\Analytics\Data\V1beta\Filter;
use Google\Analytics\Data\V1beta\FilterExpression;
use Google\Analytics\Data\V1beta\Metric;
use Google\Analytics\Data\V1beta\OrderBy;
use Google\Analytics\Data\V1beta\RunReportResponse;


/**
 * Class ConnectionService for Google data api.
 */
class ConnectionService implements ContainerAwareInterface, ConnectionServiceInterface {

  use ContainerAwareTrait;

  /**
   * Drupal\Core\Config\ImmutableConfig definition.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function request(string $page_path = '') {
    try {
      $this->setKeyLocation();
      // Initialize analytics.
      /** @var \Google\Analytics\Data\V1beta\BetaAnalyticsDataClient $google_client */
      $google_client = new BetaAnalyticsDataClient();
    }
    catch (\Throwable $e) {
      $this->logger()
        ->get('easy_google_analytics_counter')
        ->error('Process update page views from google analytics stopped. ' . $e->getMessage());
      return;
    }

    // Your view ID, defined on admin form.
    $view_id = $this->config()->get('view_id');

    // Prepare google dimensions.
    $dimension = $this->config()->get('sort_dimension');
    $dimensions = [
      new Dimension([
        'name' => 'pagePath',
      ]),
    ];
    if (strtolower($dimension) !== 'screenpageviews') {
      $dimensions[] = new Dimension([
        'name' => $dimension,
      ]);
    }

    // Create DateRange.
    $days = $this->config()->get('start_date');
    $date_ranges = [
      new DateRange([
        'start_date'  => $days . 'daysAgo',
        'end_date'    => 'today',
      ]),
    ];

    $metrics = [
      new Metric([
        'name' => 'screenPageViews'
      ]),
    ];

    // Order by page views.
    /** @var \Google\Analytics\Data\V1beta\OrderBy $order_by */
    $order_by = new OrderBy([
      'metric' => new OrderBy\MetricOrderBy([
        'metric_name' => 'screenPageViews',
      ]),
    ]);
    $order_by->setDesc(TRUE);
    $order_bys = [
      $order_by,
    ];

    // Add page path filter.
    if (!empty($page_path)) {
      $filter_expression = new FilterExpression([
        'filter' => new Filter([
          'field_name'    => 'pagePath',
          'string_filter' => new Filter\StringFilter([
            'value'       => $page_path,
            'operation'   => Filter\NumericFilter\Operation::EQUAL,
          ]),
        ]),
      ]);
    }

    $query = [
      'property' => 'properties/' . $view_id,
      'dateRanges' => $date_ranges,
      'dimensions' => $dimensions,
      'metrics' => $metrics,
      'orderBys' => $order_bys,
      'Limit' => $this->config()->get('number_items'),
    ];
    if (isset($filter_expression)) {
      $query['metricFilter'] = $filter_expression;
    }

    // Allow modules to query altering.
    $this->moduleHandler()->alter('easy_google_analytics_counter_query', $query);

    try {
      $response = $google_client->runReport($query);
      $this->setData($response);
    }
    catch (\Throwable $e) {
      $this->logger()
        ->get('easy_google_analytics_counter')
        ->error($e->getMessage());
    }
  }

  /**
   * Set pageviews data to database.
   *
   * @param \Google_Service_AnalyticsReporting_GetReportsResponse $response
   *   The response from GA.
   */
  protected function setData(RunReportResponse $response) {
    $ga_views = [];
    $fp = NULL;
    $debug = $this->config()->get('debug');
    /** @var \Google\Protobuf\Internal\RepeatedField $dimension_headers */
    $dimension_headers = $response->getDimensionHeaders();
    $alias_manager = $this->container->get('path_alias.manager');
    if ($debug === 1) {
      $url = $this->container->get('file_system')
        ->realpath('public://ga_alias_file.csv');
      $fp = fopen($url, 'wb');
      fputcsv($fp, ['Page views', 'GA path', 'System path']);
    }
    /** @var \Google\Analytics\Data\V1beta\Row $row */
    foreach ($response->getRows() as $row) {
      $matches = [];
      /** @var \Google\Protobuf\Internal\RepeatedField $dimensions */
      $dimensions = $row->getDimensionValues();
      /** @var \Google\Protobuf\Internal\RepeatedField $metrics */
      $metrics = $row->getMetricValues();
      /** @var \Google\Analytics\Data\V1beta\DimensionHeader $dimension_header */
      foreach ($dimension_headers as $i => $dimension_header) {
        if ($dimension_header->getName() === 'pagePath') {
          $dimension_value = $dimensions->offsetGet($i)->getValue();
          $alias = $alias_manager
            ->getPathByAlias(strstr($dimension_value, '?', 1));
          if (empty($alias)) {
            $alias = $alias_manager->getPathByAlias($dimension_value);
          }
          $page_views = $metrics->offsetGet($i)->getValue();
          if (preg_match('/node\/(\d+)(?<!\/)$/', $alias, $matches) &&
            !empty($matches[1]) && !empty($page_views)) {
            // Merge different aliases for same node id.
            if (!isset($ga_views[$matches[1]])) {
              $ga_views[$matches[1]] = $page_views;
            }
            else {
              $ga_views[$matches[1]] += $page_views;
            }
          }
          if ($fp !== NULL) {
            // Put data to csv file.
            fputcsv($fp, [$page_views, $dimension_value, $alias]);
          }
        }
      }
    }

    // Write results to database.
    $this->updateNodePageViews($ga_views);

    // Results logging.
    $context = [
      '%count' => count($ga_views),
      '%elements' => http_build_query($ga_views, ' ', ';'),
    ];
    $this->logger()
      ->get('easy_google_analytics_counter')
      ->notice('updated %count elements page views from Google Analytics: %elements', $context);

    if ($fp !== NULL) {
      // Close csv file.
      fclose($fp);
    }
  }

  /**
   * Write results to database.
   *
   * @param array $ga_views
   *   The node page views array.
   */
  protected function updateNodePageViews(array $ga_views) {
    $update_nids = [];
    $database = $this->container->get('database');
    foreach ($ga_views as $nid => $value) {
      $query = $database->update('node_field_data')
        ->fields(['page_views' => $value])
        ->condition('nid', $nid);
      /** @var \Drupal\Core\Database\Query\ConditionInterface $or */
      $or = $query->orConditionGroup();
      $or->condition('page_views', $value, '<>')
        ->isNull('page_views');
      $query->condition($or);
      $updated = $query->execute();
      if ($updated > 0) {
        $update_nids[$nid] = $value;
      }
    }
    if (!empty($update_nids)) {
      // Invalidate the EGAC page views cache tag.
      $this->container->get('cache_tags.invalidator')
        ->invalidateTags(['easy_google_analytics_counter_page_views']);
      // Let other modules to use google aanalytics views result.
      $this->moduleHandler()
        ->invokeAll('easy_google_analytics_counter_update_node_page_views', [$update_nids]);
    }
  }

  /**
   * Set env for key file location.
   */
  protected function setKeyLocation() {
    $url = $this->config()->get('service_account_credentials_json_path');
    if (!$url) {
      $files = $this->config()->get('service_account_credentials_json');
      /** @var \Drupal\file\FileInterface $file */
      if (!empty($files[0]) &&
        $file = $this->container->get('entity_type.manager')
          ->getStorage('file')->load($files[0])) {
        $url = $this->container->get('file_system')
          ->realpath($file->getFileUri());
      }
    }

    // Set the path to our credentials JSON file.
    // IMPORTANT: Do not place this in a web accessible location!
    putenv('GOOGLE_APPLICATION_CREDENTIALS=' . $url);
  }

  /**
   * @inheritDoc
   */
  public function config(): ImmutableConfig {
    if ($this->config === NULL) {
      $this->config = $this->container->get('config.factory')
        ->get('easy_google_analytics_counter.admin');
    }

    return $this->config;
  }

  /**
   * @inheritDoc
   */
  public function logger(): LoggerChannelFactoryInterface {
    if ($this->logger === NULL) {
      $this->logger = $this->container->get('logger.factory');
    }

    return $this->logger;
  }

  /**
   * @inheritDoc
   */
  public function moduleHandler(): ModuleHandlerInterface {
    if ($this->moduleHandler === NULL) {
      $this->moduleHandler = $this->container->get('module_handler');
    }

    return $this->moduleHandler;
  }

}
